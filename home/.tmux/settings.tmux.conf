#=============================================================================#
# Settings
#=============================================================================#

# Set default
set -g default-command /bin/zsh

# set -g default-command "reattach-to-user-namespace -l zsh"

# 256 Colors UTF8
set -g default-terminal "screen-256color"

# setw -g utf8 on
# set-window-option -g utf8 on
# set -g status-utf8 on

set -g history-limit 20000

# automatically renumber tmux windows
set -g renumber-windows on

# Activity Monitoring
setw -g monitor-activity off
set -g visual-activity off

# Rather than constraining window size to the maximum size of any client
# connected to the *session*, constrain window size to the maximum size of any
# client connected to *that window*. Much more reasonable.
setw -g aggressive-resize on

# make delay shorter
set -sg escape-time 0

# make window/pane index start with 1
set -g base-index 1
setw -g pane-base-index 1

# set-option -g set-titles on
# set-option -g set-titles-string "#T - #W"
# set-window-option -g automatic-rename on

# enable mouse support for switching panes/windows
# NOTE: This breaks selecting/copying text on OSX
# To select text as expected, hold Option to disable it (iTerm2)
set -g mouse on
bind -n WheelUpPane if-shell -F -t = "#{mouse_any_flag}" "send-keys -M" "if -Ft= '#{pane_in_mode}' 'send-keys -M' 'select-pane -t=; copy-mode -e; send-keys -M'"
bind -n WheelDownPane select-pane -t= \; send-keys -M
bind -n C-WheelUpPane select-pane -t= \; copy-mode -e \; send-keys -M
bind -t vi-copy    C-WheelUpPane   halfpage-up
bind -t vi-copy    C-WheelDownPane halfpage-down
bind -t emacs-copy C-WheelUpPane   halfpage-up
bind -t emacs-copy C-WheelDownPane halfpage-down

# set vi mode for copy mode
setw -g mode-keys vi

# session name to iterm2 tab name
set-option -g set-titles on
set-option -g set-titles-string "#{session_name}"

# Set update to every 60seconds
# set -g status-interval 60

