" Load the vimrc to nvimrc
source ~/.dotfiles/vim/defaults.vim
source ~/.dotfiles/vim/plugins.neo.vim
source ~/.dotfiles/vim/theme.vim
source ~/.dotfiles/vim/pluginsettings.vim
