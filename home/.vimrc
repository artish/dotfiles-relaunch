" Load the vimrc to nvimrc
source ~/.dotfiles/vim/defaults.vim

"Allow usage of mouse in iTerm
set ttymouse=xterm2