" " Theme
" set background=dark

syntax enable
set background=dark
colorscheme base16-ocean
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
highlight SpecialKey ctermfg=66 guifg=#41495A

" Restoe colorscheme for NeoVim Terminal
let g:terminal_color_0="#1b2b34"
let g:terminal_color_1="#ed5f67"
let g:terminal_color_2="#9ac895"
let g:terminal_color_3="#fbc963"
let g:terminal_color_4="#669acd"

" colorscheme OceanicNext
" let base16colorspace=256
" highlight NonText guibg=#060606
" highlight Folded  guibg=#0A0A0A guifg=#9090D0
