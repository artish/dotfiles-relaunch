" Removes trailing spaces
function! TrimWhiteSpace()
  " Don't strip whitespace in markdown documents
  if &ft =~ 'markdown\|md'
    return
  endif
  %s/\s\+$//e
endfunction

nnoremap <silent> <Leader>rts :call TrimWhiteSpace()<CR>

autocmd FileWritePre    * :call TrimWhiteSpace()
autocmd FileAppendPre   * :call TrimWhiteSpace()
autocmd FilterWritePre  * :call TrimWhiteSpace()
autocmd BufWritePre     * :call TrimWhiteSpace()
