Installation of the modules
===========================

## Brew packages

  ```bash
  brew tap Homebrew/bundle
  brew bundle
  ```

## PIP Packages

  ```bash
  pip install -r Pipfile
  ```
